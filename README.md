# Breakout [2018]

Created a Breakout game with JavaScript and HTML5

## Playable Example

[Play Breakout](https://majorvitec.itch.io/breakout-javascript)

## Game Overview

### Playing Screen

![Playing Screen](/images/readme/play_screen.png "Playing Screen")

